﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateWidget : MonoBehaviour {

    public Image p1win;
    public Image p1lose;
    public Image p2win;
    public Image p2lose;

    public Image orangeBar;
    public Image blueBar;
    public Text timer;
    public static float player1Count = 0.5f;
    public static float player2Count = 0.5f;
    public static float amountOnKill = 0.1f;
    private bool finished;

    private float timeCount = 180;

    public void Update() {
        if (!finished) {
            UpdateTimer();
            blueBar.fillAmount = player1Count;
            orangeBar.fillAmount = player2Count;
        }
    }

    private void UpdateTimer() {
        timer.text = string.Format("{0}:{1:00}", (int)timeCount / 60, (int)timeCount % 60);
        timeCount -= Time.deltaTime;

        if (timeCount <= 0) {
            TimeEnd();
        }
    }

    public void TimeEnd() {
        if(player1Count > player2Count)
            Victory2();
        else
            Victory1();
    }

    public void PlayerDie(int id) {
        if (id == 0) {
            player1Count += amountOnKill;
            player2Count -= amountOnKill;
        } else if (id == 1) {
            player1Count -= amountOnKill;
            player2Count += amountOnKill;
        }

        if (player1Count <= 0)
            Victory2();

        if (player2Count <= 0)
            Victory1();
    }

    public void Victory1() {
        p1win.gameObject.SetActive(true);
        p2lose.gameObject.SetActive(true);
        finished = true;
    }

    public void Victory2() {
        p1win.gameObject.SetActive(true);
        p2lose.gameObject.SetActive(true);
        finished = true;
    }
}
