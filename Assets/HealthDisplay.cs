﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour {

    public Text text;
    public PlayerController player;

    private void Update() {
        text.text = player.currentHealth.ToString();
    }
}
