﻿using System.Collections;
using System.Collections.Generic;
using Rewired.Platforms.Custom;
using UnityEngine;

public class ShootCone : MonoBehaviour {

    public PlayerController character;
    private Animator anim;

    public void Start() {
        anim = GetComponent<Animator>();
    }

    private void Update() {
        anim.SetBool("Shooting", character.player.GetButton("Shoot"));
    }

}
