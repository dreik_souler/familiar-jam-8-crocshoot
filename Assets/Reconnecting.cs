﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reconnecting : MonoBehaviour {

    public PlayerController player;
    public CanvasGroup group;

	// Use this for initialization
	private void Update () {
	    if (player._reconnect)
	        group.alpha = 1;
	    else
	        group.alpha = 0;
	}
}
