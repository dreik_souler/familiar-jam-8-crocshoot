﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sight : MonoBehaviour {

    public bool left;
    public bool right;

    private void Start() {
        if (left) {
            float x = Screen.width / 4f;
            float y = Screen.height / 2f;
            transform.position = new Vector3(x, y, 0);
        }

        if (right) {
            float x = Screen.width / 4f * 3;
            float y = Screen.height / 2f;
            transform.position = new Vector3(x, y, 0);
        }
    }
}
