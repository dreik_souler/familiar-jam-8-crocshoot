﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class SpawnPoint : MonoBehaviour {

    private static List<Vector3> spawns = new List<Vector3>();

    private void Start() {
        spawns.Add(transform.position);
    }

    public static Vector3 GetSpawn() {
        int index = Random.Range(0, spawns.Count);
        return spawns[index];
    }
}
