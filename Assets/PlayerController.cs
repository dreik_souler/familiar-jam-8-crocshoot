﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    // Player 1 controls
    public const string Player1Horizontal = "Horizontal1";
    public const string Player1Vertical = "Vertical1";
    public const string Player1HorizontalRight = "HorizontalR1";
    public const string Player1VerticalRight = "VerticalR1";
    public const string Player1Jump = "Jump1";
    public const string Player1Shoot = "Shoot1";

    // Player2 controls
    public const string Player2Horizontal = "Horizontal2";
    public const string Player2Vertical = "Vertical2";
    public const string Player2HorizontalRight = "HorizontalR2";
    public const string Player2VerticalRight = "VerticalR2";
    public const string Player2Jump = "Jump2";
    public const string Player2Shoot = "Shoot2";

    public Animator anim;
    public GameStateWidget state;
    public GameObject aim;
    public CameraShake shake;
    public Camera myCamera;
    public Player player;
    public int playerId;
    public float health;
    public float speed;
    public float jumpSpeed;
    public float gravity;
    public float currentHealth;
    private Vector3 moveDirection;
    private Rigidbody rigidBody;
    private CharacterController controller;
    private bool controllable;
    private bool dead;
    public bool canRotate = true;
    public bool _lag = false;
    float timeLag = 0.0f;
    float timeGetLag = 0.0f;
    private Vector3 posLag = Vector3.zero;
    private bool _lagPos = false;

    public bool _reconnect = false;
    private float timeReconnect = 0.0f;
    [SerializeField]
    private float maxTimeReconnect = 4;

    public AudioClip stepFoot;

    public bool _aim = false;
    private float tStep = 0;

    private void Start() {
        rigidBody = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        myCamera = GetComponentInChildren<Camera>();
        if(playerId == 0)
            myCamera.rect = new Rect(0.0f, 0.0f, 0.5f, 1.0f);
        else if(playerId == 1)
            myCamera.rect = new Rect(0.5f, 0.0f, 0.5f, 1.0f);
        controllable = true;
        currentHealth = health;
        player = ReInput.players.GetPlayer(playerId);
    }

    private void Update () {
        #region LAG
        if (_lag)
        {
            timeLag -= Time.deltaTime;
            if (timeLag > 0)//Si hay tiempo de lag
            {
                if (timeLag <= timeGetLag)
                {
                    if (!_lagPos)
                    {
                        _lagPos = true;
                        posLag = transform.position;
                    }else
                    {
                        transform.position = posLag;
                        _lagPos = false;
                    }
                    timeGetLag -= 0.5f;
                }
            }
            else
                _lag = false;
            
        }
        #endregion

        _aim = !canRotate;

        #region RECONNECT
        if (_reconnect)
        {
            //mostrar icono
            timeReconnect += Time.deltaTime;
            if (timeReconnect >= maxTimeReconnect)
            {
                _reconnect = false;
                //Ocultar
            }
        }
        #endregion

        tStep += Time.deltaTime;

        if (playerId == 0)
            MovePlayer();
        else if(playerId == 1)
            MovePlayer();

        if (canRotate)
        {
            if (playerId == 0)
                MoveCamera();
            else if (playerId == 1)
                MoveCamera();
        }

        aim.GetComponent<CameraController>().aim = player.GetButton("Aim");


    }

    private void MovePlayer() {
        if (controller.isGrounded && controllable) {
            anim.SetBool("Moving", player.GetAxis("Horizontal") != 0 && player.GetAxis("Vertical") != 0);
            moveDirection = new Vector3(player.GetAxis("Horizontal"), 0, player.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (moveDirection.x!= 0 || moveDirection.z != 0)
            {
                if (tStep > 0.5f)
                {
                    GameObject.FindGameObjectWithTag("SoundMusicManager").GetComponent<SoundManager>().play(stepFoot);
                    tStep = 0;
                }

            }
            if (player.GetButton("Jump")) {
                moveDirection.y = jumpSpeed;

            }
            anim.SetBool("Jumping", !controller.isGrounded);

        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    private void MoveCamera() {
        float _speedR = 3.0f;
        float xRot = _speedR * -player.GetAxis("VerticalRight");
        float yRot = _speedR * player.GetAxis("HorizontalRight");
        //Si le pongo los dos ejes rota el personaje, por lo tanto tendríamos que separar el control
        transform.Rotate(0.0f, yRot, 0.0f);
        aim.transform.Rotate(xRot, 0, 0);
    }

    public void Damage(float damage) {
        if (damage >= currentHealth)
            Kill();
        else if (damage < currentHealth)
            currentHealth -= damage;
        shake.Shake();

        foreach(Joystick j in player.controllers.Joysticks) {
            if(!j.supportsVibration) continue;
            if(j.vibrationMotorCount > 0) j.SetVibration(0, 1, 1.0f); // 1 second duration
        }
    }

    public void Heal(float heal) {
        if (heal >= health)
            currentHealth = heal;
        else
            currentHealth += heal;
    }

    private void Kill() {
        currentHealth = 0;
        dead = true;
        controllable = false;
        Respawn();
        state.PlayerDie(playerId);
    }

    private void Respawn() {
        transform.position = SpawnPoint.GetSpawn();
        dead = false;
        currentHealth = health;
        StartCoroutine(FullHealth());
        controllable = true;
    }

    private IEnumerator FullHealth() {
        yield return new WaitForSeconds(0.1f);
        currentHealth = health;
    }

    public void rotate(bool canRotate)
    {
        this.canRotate = canRotate;
    }

    public void lag()
    {
        //Tenemos lag y no hemos capturado la poscion del lag
        _lag = true;
        timeLag = Random.Range(5, 8);
        _lagPos = false;
        timeGetLag = timeLag - 0.5f;
    }

    public void reconnect()
    {
        _reconnect = true;
        timeReconnect = 0;

    }
}
