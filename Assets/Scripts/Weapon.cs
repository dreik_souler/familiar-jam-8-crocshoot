﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Weapon : MonoBehaviour {

    public delegate void ShootEvent(int id);
    public static event ShootEvent OnShoot;

    // Editor variables
    public Animator animator;
    public PlayerController player;
    public GameObject hole;
    [Range(1, 100)]
    public float damage;
    [Range(0.1f, 3)]
    public float cadency;
    [Range(1, 100)]
    public int clipsCapacity;
    [Range(0.5f, 5)]
    public float reloadTime;
    public AudioClip clip;
    // State variables
    private int currentClips;
    private Ray ray;
    private bool reloading;
    private float shootCooldown;
    public ShootCone cone;

    private void Start() {

    }

    private void Update() {
        CooldownUpdate();
        if(player.playerId == 0)
            Shoot();
        else if(player.playerId == 1)
            Shoot();
    }

    private void CooldownUpdate() {
        if (shootCooldown > 0)
            shootCooldown -= Time.deltaTime;
        else if (shootCooldown < 0)
            shootCooldown = 0;
    }

    public void Shoot() {
        if (HasCooldown() || reloading || shootCooldown > 0) return;

        float shootKey = player.player.GetAxis("Shoot");
        animator.SetBool("SHOOTING", shootKey > 0);
        float raycastX = 0;
        float raycastY = 0;

        if (player.playerId == 0) {
            raycastX = Screen.width / 4f;
            raycastY = Screen.height / 2f;
        } else if (player.playerId == 1) {
            raycastX = Screen.width / 2f + Screen.width / 4f;
            raycastY = Screen.height / 2f;
        }

        ray = player.myCamera.ScreenPointToRay(new Vector2(raycastX, raycastY));
        RaycastHit[] hits = Physics.RaycastAll(ray);

        if (shootKey == 1) {
            GameObject.FindGameObjectWithTag("SoundMusicManager").GetComponent<SoundManager>().play(clip);
            ResetCooldown();

            for (int i = 0; i < hits.Length; i++) {
                RaycastHit hit = hits[i];
                if (hit.collider.gameObject == player.gameObject)
                    continue;

                if (hit.collider.CompareTag("Player"))
                    OnPlayerHit(hits[i]);
                else if (hit.collider.CompareTag("Level"))
                    OnLevelHit(hits[i]);
            }
        }
    }

    private void OnPlayerHit(RaycastHit hit) {
        hit.collider.GetComponent<PlayerController>().Damage(damage);
    }

    private void OnLevelHit(RaycastHit hit) {
        GameObject go = Instantiate(hole, hit.point, hit.transform.rotation);
        float translationY = Random.Range(0.00000001f, 0.01f);
        go.transform.Translate(0, translationY, 0);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(ray.origin, ray.direction);
        Gizmos.DrawLine(ray.origin, ray.origin + ray.direction * 100);
    }

    private bool HasCooldown() {
        return shootCooldown > 0;
    }

    private void ResetCooldown() {
        shootCooldown = cadency;
    }
}
