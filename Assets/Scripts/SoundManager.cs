﻿using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource soundSource;
    public AudioSource musicSource;

    // Use this for initialization
    void Start()
    {
        //PlayMusicMenu();
    }

    public void play(AudioClip clip)
    {
        if (soundSource != null && clip != null)
            soundSource.PlayOneShot(clip);
    }
    /*
    public void PlayMusicMenu()
    {
        musicSource.Stop();
        musicSource.clip = musicMenu;
        musicSource.Play();
    }

    public void PlayMusicGame()
    {
        musicSource.Stop();
        musicSource.clip = musicGame;
        musicSource.Play();
    }*/
}
