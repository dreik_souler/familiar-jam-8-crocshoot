﻿using System.Collections;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public float lerpRatio = 0.25f;
    public float min = -0.3f;
    public float max = 0.3f;

    public void Shake() {
        float x = Random.Range(min, max);
        float y = Random.Range(min, max);
        float z = Random.Range(min, max);
        transform.localPosition = new Vector3(x, y, z);
    }

    private void Update() {
        transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * lerpRatio);
    }

}
