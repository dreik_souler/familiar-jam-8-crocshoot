﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpPlatform : MonoBehaviour {

    // Editor variables
    public int spawnTime;
    public GameObject[] powerUps;

    // State variables
    private float respawnCooldown;

    private void Start() {
        SpawnItem();
        respawnCooldown = spawnTime;
    }

    private void Update() {
        UpdateCooldown();
    }

    public void SpawnItem() {
        GameObject go = Instantiate(GetRandomPowerUp());
        go.transform.parent = transform;
        go.transform.localPosition = new Vector3(0, 3f, 0);
    }

    private void UpdateCooldown() {
        if (IsPowerupSpawned()) return;
        if (respawnCooldown > 0)
            respawnCooldown -= Time.deltaTime;
        else {
            respawnCooldown = spawnTime;
            SpawnItem();
        }
    }

    private GameObject GetRandomPowerUp() {

        int index = Random.Range(0, powerUps.Length);
        GameObject powerUp = powerUps[index];

        if (powerUp == null)
            return new GameObject();
        else
            return powerUp;
    }

    public bool IsPowerupSpawned() {
        for (int i = 0; i < transform.childCount; i++) {
            if (transform.GetChild(i).CompareTag("PowerUp"))
                return true;
        }
        return false;
    }


}
