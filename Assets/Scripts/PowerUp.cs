﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum PowerUpStates { LAG, AIM, HEALTH, MOUSE, RECONNECT };
public class PowerUp : MonoBehaviour {
    public PowerUpStates state;
    public float rotationSpeed;

    private void Start() {

    }

    private void Update() {
        Rotate();
    }

    private void OnTriggerEnter(Collider col) {
        if (col.tag == "Player") {
            col.GetComponent<PlayerStates>().getPowerUps(state);
            Pickup();
        }
    }

    private void Rotate() {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }

    private void Pickup() {
        Destroy(transform.parent.gameObject);
    }

}
