﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    private static float xDistance = 0.4f;
    bool leftSide = true;
    public float distance = -1;
    public bool changing = false;
    public bool fading = false;
    private float t = 0;
    private float t2 = 0;
    public bool aim = true;
    public bool previousAim = true;
    public Vector3 aimPosL = Vector3.zero;
    public Vector3 aimPosR = Vector3.zero;
    public Vector3 aimPosLBack = Vector3.zero;
    public Vector3 aimPosRBack = Vector3.zero;
    // Use this for initialization
    void Start () {
        transform.GetChild(0).localPosition =new Vector3(leftSide?-xDistance:xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z);
        aimPosL = new Vector3(-xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z);
        aimPosR = new Vector3(xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z);
        aimPosLBack = aimPosL + new Vector3(0, 0.5f, -2);
        aimPosRBack = aimPosR + new Vector3(0, 0.5f, -2);

    }

    // Update is called once per frame
    void Update () {
        controlRotate();
        if ( aim != previousAim)
        {
            fading = true;
            previousAim = aim;
        }
        
        if (fading)
        {
            if (leftSide)
            {
                float rateTiempo2 = 1f / 0.1f;//1 / tiempo total
                float rateVelocity2 = 1f / Vector3.Distance(aim ? aimPosLBack : aimPosL, aim ? aimPosL : aimPosLBack);

                if (t2 <= 1f)
                {
                    t2 += Time.deltaTime * rateTiempo2;
                    transform.GetChild(0).localPosition = Vector3.Lerp(aim ? aimPosLBack : aimPosL, aim ? aimPosL : aimPosLBack, t2);
                }
                else
                {
                    fading = false;
                    t2 = 0;
                }
            }else
            {
                float rateTiempo2 = 1f / 0.1f;//1 / tiempo total
                float rateVelocity2 = 1f / Vector3.Distance(aim ? aimPosRBack : aimPosR, aim ? aimPosR : aimPosRBack);

                if (t2 <= 1f)
                {
                    t2 += Time.deltaTime * rateTiempo2;
                    transform.GetChild(0).localPosition = Vector3.Lerp(aim ? aimPosRBack : aimPosR, aim ? aimPosR : aimPosRBack, t2);
                }
                else
                {
                    fading = false;
                    t2 = 0;
                }
            }

        }
	}

    public void controlRotate()
    {
        float angulo = transform.localEulerAngles.x;
        //Debug.Log("EULER= "+transform.eulerAngles.x);
        int i = 0;
        while (angulo> 180)
        {   angulo -= 360;   i++; if (i < 5) break; }
        while (angulo < -180)
        { angulo += 360; i++; if (i < 5) break; }
        //Debug.Log("Angulo: " + angulo);

        if (angulo > 60)
        {
            transform.localEulerAngles = new Vector3(60, transform.localEulerAngles.y, transform.localEulerAngles.z);
        }
        if (angulo < -60 )
        {
            transform.localEulerAngles = new Vector3(-60, transform.localEulerAngles.y, transform.localEulerAngles.z);
        }
    }
    private void LateUpdate()
    {
        if (changing)
        {
            float rateTiempo = 1f / 0.2f;//1 / tiempo total
            float rateVelocity = 1f / Vector3.Distance(new Vector3(leftSide ? xDistance : -xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z), new Vector3(leftSide ? -xDistance : xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z));

            if (t <= 1f)
            {
                t += Time.deltaTime * rateTiempo;
                transform.GetChild(0).localPosition = Vector3.Lerp(new Vector3(leftSide ? xDistance : -xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z), new Vector3(leftSide ? -xDistance : xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z), t);
            }
            else
            {
                changing = false;
            }
        }
        RaycastHit hit = new RaycastHit();
        Vector3 _posIniticial = transform.position;
        Vector3 _dir = transform.GetChild(0).transform.position - transform.position;
        _dir.Normalize();
        _posIniticial += _dir;
        if (Physics.Raycast(_posIniticial, _dir, out hit))//Back
        {
            distance = hit.distance;
            //Debug.Log("Distance: " + distance +" "+ hit.transform.gameObject.name);
            //transform.position = hit.point;
        }
        //Laterales
        if (Physics.Raycast(transform.GetChild(0).position, leftSide?-transform.GetChild(0).right: transform.GetChild(0).right, out hit)) {
            if (hit.collider.CompareTag("Level"))
                return;

            distance = hit.distance;
            if (distance< 0.5f)
            {
                //changeSide();
            }
            //Debug.Log("LeftDistance: " + distance + " " + hit.transform.gameObject.name);
        }
    }
    private void changeSide()
    {
        changing = true;
        leftSide = !leftSide;
        t = 0;
        //transform.GetChild(0).localPosition = new Vector3(leftSide ? -xDistance : xDistance, transform.GetChild(0).localPosition.y, transform.GetChild(0).localPosition.z);
    }
}
