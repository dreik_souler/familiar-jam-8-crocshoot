﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStates : MonoBehaviour {
    public GameObject enemy;
    private PlayerController playerScript;
    private float timerMouse = 0;
    private float maxTimerMouse = 3f;
	// Use this for initialization
	void Start () {
        playerScript = GetComponent<PlayerController>();
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (item != gameObject)
            {
                enemy = item;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (timerMouse < maxTimerMouse)
        {
            timerMouse += Time.deltaTime;
            if (timerMouse > maxTimerMouse)
            {
                playerScript.rotate(true);
            }
        }
	}

    public void getPowerUps(PowerUpStates powerUp)
    {
        switch (powerUp)
        {
            case PowerUpStates.LAG:
                enemy.GetComponent<PlayerController>().lag();
                break;
            case PowerUpStates.AIM:
                break;
            case PowerUpStates.HEALTH:
                GetComponent<PlayerController>().Heal(50);
                break;
            case PowerUpStates.MOUSE:
                enemy.GetComponent<PlayerStates>().noMouse();
                break;
            case PowerUpStates.RECONNECT:
                enemy.GetComponent<PlayerController>().reconnect();
                break;
            default:
                break;
        }
    }

    public void noMouse()
    {
        playerScript.rotate(false);
        timerMouse = 0;
    }
}
